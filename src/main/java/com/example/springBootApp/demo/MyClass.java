package com.example.springBootApp.demo;

import graphql.kickstart.spring.webclient.boot.GraphQLRequest;
import graphql.kickstart.spring.webclient.boot.GraphQLResponse;
import graphql.kickstart.spring.webclient.boot.GraphQLWebClient;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
class MyClass {

    private final GraphQLWebClient graphQLWebClient;

    MyClass(GraphQLWebClient graphQLWebClient) {
        this.graphQLWebClient = graphQLWebClient;
    }

    CountryDto helloWorld() throws IOException {

//        final String query = GraphqlSchemaReaderUtil.getSchemaFromFileName("getCountryDetails");
//        GraphQLRequest request = GraphQLRequest.builder().query(query).build();

        final String query = GraphqlSchemaReaderUtil.getSchemaFromFileName("aa");
        final String var = GraphqlSchemaReaderUtil.getSchemaFromFileName("variables");
        GraphQLRequest request = GraphQLRequest.builder().query(query).variables(var).build();


        GraphQLResponse response = graphQLWebClient.post(request).block();
        final CountryDto country = response.get("country", CountryDto.class);


        final String query2 = GraphqlSchemaReaderUtil.getSchemaFromFileName("getCountries");
        GraphQLRequest request2 = GraphQLRequest.builder().query(query2).build();
        GraphQLResponse response2 = graphQLWebClient.post(request2).block();
        final List<CountryDto> countries = response2.getList("countries", CountryDto.class);




        return country;
    }
}
