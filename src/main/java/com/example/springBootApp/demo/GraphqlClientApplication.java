package com.example.springBootApp.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;

@SpringBootApplication
@Slf4j
public class GraphqlClientApplication {




  public static void main(String[] args) throws IOException {

    ConfigurableApplicationContext context = SpringApplication.run(GraphqlClientApplication.class, args);
//    MyClass client = (MyClass) context.getBean("myClass");
//    MyClassDos client = (MyClassDos) context.getBean("myClassDos");
//    final CountryDto countryDto = client.helloWorld();

    MyClassWebClient client = (MyClassWebClient) context.getBean("myClassWebClient");
    final CountryDto countryDto = client.helloWorld("BE");

    ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
    log.info(ow.writeValueAsString(countryDto));





//
//    ConfigurableApplicationContext context = SpringApplication.run(GraphqlClientApplication.class, args);
//    CountryClient client = (CountryClient) context.getBean("countryClient");
//    ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
//    CountryDto countryDto = client.getCountryDetails("BE");
//    log.info(ow.writeValueAsString(countryDto));


  }
}
