package com.example.springBootApp.demo;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.IOException;

@Component
class MyClassWebClient {

    CountryDto helloWorld(String countryCode) throws IOException {

        WebClient webClient = WebClient.builder().baseUrl("https://countries.trevorblades.com/").build();

        GraphqlRequestBody graphQLRequestBody = new GraphqlRequestBody();

        final String query = GraphqlSchemaReaderUtil.getSchemaFromFileName("aa");
        final String variables = GraphqlSchemaReaderUtil.getSchemaFromFileName("variablesAnother");

        graphQLRequestBody.setQuery(query);
        graphQLRequestBody.setVariables(variables.replace("countryCode", countryCode));

        final CountryDtoWrapper countryDtoWrapper = webClient.post()
                                                 .bodyValue(graphQLRequestBody)
                                                 .retrieve()
                                                 .bodyToMono(CountryDtoWrapper.class)
                                                 .block();
        CountryDto country = countryDtoWrapper.getData().getCountry();
        return country;
    }


}
