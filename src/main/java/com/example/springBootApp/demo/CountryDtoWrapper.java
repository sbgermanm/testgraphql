package com.example.springBootApp.demo;

import lombok.Getter;

@Getter
public class CountryDtoWrapper {

  private CountryData data;

  @Getter
  public class CountryData {

    private CountryDto country;

  }
}

