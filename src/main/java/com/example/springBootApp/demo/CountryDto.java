package com.example.springBootApp.demo;

import lombok.Getter;

@Getter
public class CountryDto {


      private String name;
      private String capital;
      private String currency;
}

